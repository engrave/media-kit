# Engrave Witness Media Kit

## Engrave witness banner

### SVG 756px
![](./banner_engrave_756.svg)

### PNG (756px width)
![](./banner_engrave_756.png)

### SVG 640px
![](./banner_engrave_640.svg)

### PNG (640px width)
![](./banner_engrave_640.png)

***

## Sketch project: [link](./banner_engrave.sketch)
